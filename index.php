<!DOCTYPE html>
<html lang="en">
<head>
  <title>Simulator Merchant MPayment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    var n = 1;
    function AddItem()
    {
      n += 1;
      document.getElementById("cart").innerHTML += '<div id="cart'+n+'" class="cart"><b>Item '+n+'</b><br>Harga: <input type="number" id="price'+n+'" name="price" value="10000"> Qty: <input type="number" id="qty'+n+'" name="qty" value="1"><button type="button" onclick="RemoveItem('+n+')">Remove</button></div>';
    }

    function RemoveItem(c)
    {
        var elem = document.getElementById('cart' + c);
        elem.parentNode.removeChild(elem);
        return false;
    }

    function CalculateTotal()
    {
      var total = 0;
      itemCount = document.querySelectorAll(".cart").length;
      for(i=1; i<itemCount + 1; i++)
      {
        var price = document.getElementById('price' + i).value;
        var qty = document.getElementById('qty' + i).value;
        total += price * qty;
      }
      document.getElementById("total").value = total;
    }

  </script>
</head>
<body>

<div class="container">
  <h2>Checkout</h2>
  <form action="process.php/f=process_input" method="POST">
    <fieldset>
      <legend>Keranjang</legend>
      <div>
         <button type="button" onclick="AddItem()">Add</button>
      </div>
      <br>
      <div id="cart">
        <div id="cart1" class="cart">
          <b>Item 1</b>
          <br>
          Harga: <input type="number" id="price1" name="price" value="10000">
          Qty: <input type="number" id="qty1" name="qty" value="1">
          <button type="button" onclick="RemoveItem(1)">Remove</button>
        </div>
      </div>
      <br>
      <div>
         <button type="button" onclick="CalculateTotal()">Calculate Total</button>
      </div>
    </fieldset>
    <fieldset>
      <legend>Total:</legend>
        <input type="text" id="total" name="total" value="10000" readonly>
    </fieldset>
    <br>
    <fieldset>
      <legend>Informasi:</legend>
      Nomor HP:<br>
      <input type="text" name="deviceCode">
      <br>
      <br>
      Bank:<br>
      <select name="bankName">
        <option value="MDR">Mandiri</option>
        <option value="MYP">Mayapada</option>
        <option value="MYB">Maybank</option>
      </select>
      <br>
      <br>
      Nomor Rekening:<br>
      <input type="text" name="account">
      <br><br>
      <input type="submit" value="Send">
    </fieldset>
  </form>
</div>

</body>
</html>
